###############################################################################
#         University of Hawaii, College of Engineering
# @brief  Lab 07d- Animal Farm 1 - EE 205 - Spr 2022
#
# @file    Makefile
# @version 1.0
#
# @author Leo Liang <leoliang@hawaii.edu>
# @date   1_Mar_2022
#
# @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

TARGET = animalFarm


all:  $(TARGET)


CC     = gcc
CFLAGS = -Wall -Wextra $(DEBUG_FLAGS)


debug: DEBUG_FLAGS = -g -DDEBUG
debug: clean $(TARGET)

catDatabase.o: catDatabase.c catDatabase.h
	$(CC) $(CFLAGS) -c catDatabase.c catDatabase.h

addCats.o: addCats.c addCats.h
	$(CC) $(CFLAGS) -c addCats.c addCats.h

deleteCats.o: deleteCats.c deleteCats.h
	$(CC) $(CFLAGS) -c deleteCats.c deleteCats.h

reportCats.o: reportCats.c reportCats.h
	$(CC) $(CFLAGS) -c reportCats.c reportCats.h

updateCats.o: updateCats.c updateCats.h
	$(CC) $(CFLAGS) -c updateCats.c updateCats.h

main.o: config.h main.c
	$(CC) $(CFLAGS) -c main.c config.h

animalFarm.o: addCats.c catDatabase.c deleteCats.c updateCats.c reportCats.c main.c
	$(CC) $(CFLAGS) -c addCats.c catDatabase.c deleteCats.c updateCats.c reportCats.c main.c


animalFarm: addCats.o catDatabase.o deleteCats.o updateCats.o reportCats.o main.o
	$(CC) $(CFLAGS) -o $(TARGET) addCats.o catDatabase.o deleteCats.o updateCats.o reportCats.o main.o


test: $(TARGET)
	./$(TARGET)


clean:
	rm -f $(TARGET) *.o
