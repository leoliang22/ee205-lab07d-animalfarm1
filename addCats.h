///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - AnimalFarm 1 - EE 205 - Spr 2022
///
/// @file addCats.h
/// @version 1.0
///
/// @author Leo Liang <leoliang@hawaii.edu>
/// @date 1_Mar_2022
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include <stdbool.h>
#include <stdio.h>

//extern enum Genders;
//extern enum Breeds;
extern float catWeight[];
extern bool is_fixed[];
extern char catNames[];
extern size_t numCats;

int addCat(const char* name, const enum Genders gender, const enum Breeds breed, const bool isFixed , const float weight, const enum Color color1, const enum Color color2, const unsigned long long licen_num);
//extern int addCat(const char* name, const char gender, const char breed, const bool isFixed , const float weight);
