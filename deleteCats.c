///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - AnimalFarm 1 - EE 205 - Spr 2022
///
/// @file deleteCats.c
/// @version 1.0
///
/// @author Leo Liang <leoliang@hawaii.edu>
/// @date 1_Mar_2022
///////////////////////////////////////////////////////////////////////////////


#include<stdio.h>
#include "catDatabase.h"
#include "deleteCats.h"
#include <string.h>

void deleteAllCats(void){
   numCats=0;
   memset(&cats, 0, sizeof(cats));
}
