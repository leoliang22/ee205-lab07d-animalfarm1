///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - AnimalFarm 1 - EE 205 - Spr 2022
///
/// @file main.c
/// @version 1.0
///
/// @author Leo Liang <leoliang@hawaii.edu>
/// @date 1_Mar_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdbool.h>
//#include "catDatabase.c"
//#ifndef CATDATABASE_C
//#define CATDATABASE_C
//#endif
//argc, *argv[]
#include "catDatabase.h"
#include "addCats.h"
//#include "catDatabase.h"
#include "deleteCats.h"
#include "reportCats.h"
#include "updateCats.h"
//extern enum genders;
//extern enum breeds;
//enum genders {UNKNOWN_GENDER, MALE, FEMALE, OTHER};
//enum breeds {UNKNOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};

int main(void){
   printf("Starting Animal Farm 0 \n");
   addCat( "Loki", MALE, PERSIAN, true, 8.5, BLACK, WHITE, 101 ) ;
   addCat( "Milo", MALE, MANX, true, 7.0, BLACK, RED, 102 ) ;
   addCat( "Bella", FEMALE, MAINE_COON, true, 18.2, BLACK, BLUE, 103 ) ;
   addCat( "Kali", FEMALE, SHORTHAIR, false, 9.2, BLACK, GREEN, 104 ) ;
   addCat( "Trin", FEMALE, MANX, true, 12.2, BLACK, PINK, 105 ) ;
   addCat( "Chili", UNKNOWN_GENDER, SHORTHAIR, false, 19.0, WHITE, BLACK, 106 ) ;
   printAllCats();
   int kali = findCat( "Kali" ) ;
   updateCatName( kali, "Chili" ) ; // this should fail
   printAllCats();
   printCat( kali );
   updateCatName( kali, "Capulet" ) ;
   updateCatWeight( kali, 9.9 ) ;
   fixCat( kali ) ;
   printCat( kali );
   updateCatCollar1(kali, BLUE);
   updateCatCollar2(kali, PINK);
   updateLicense(kali, 103);
   printAllCats();
   deleteAllCats();
   printAllCats();

   printf("Done with Animal Farm 0 \n");
}
