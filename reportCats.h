///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - AnimalFarm 1 - EE 205 - Spr 2022
///
/// @file reportCats.h
/// @version 1.0
///
/// @author Leo Liang <leoliang@hawaii.edu>
/// @date 1_Mar_2022
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include <stdbool.h>
#include <stdio.h>
#include "catDatabase.h"

extern size_t numCats;
extern float catWeight[];
extern bool is_fixed[];



extern int printCat(int index);
extern void printAllCats();
extern int findCat();
extern char* genderName(const enum Genders gender);
extern char* breedName(const enum Breeds breed);
extern char* collarColorName(const enum Color color);
